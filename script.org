#+LATEX_HEADER: \usepackage[margin=0.5in, top=1in]{geometry}
#+TITLE: Bringing IT to an English Winemaker

* TODO Presentation
** Structure
- The Client
- The Problem
- The Solution

** The Client
- English Winemaker couple Joyce and Richard Barnes
- 24 arces of land
- produce 2'000 to 10'000 bottles per year
- sell for £3 per bottle or £30 per dozen
- have shop where people watch winemakers at work
  - 20'000 visitors per year

*** Financial Situation (Estimate)
**** bad year
- costs: £304'000
- revenue: £130'000
- loss: £174'000

**** avarage year
- costs: £352'000
- revenue: £330'000
- loss: £22'000

**** this year/perfect year
- costs: £400'000
- revenue: £715'000
- earnings: £315'000

**** summary
- have money to invest, but need to save for bad times
- goal: reduce likelyhood of making a loss to allow more freedom to invest

** The Problem
- high production costs with time lag
- costs nearly unrelated to earnings
- two decisions:
  - to buy new land
  - what to do with the shop
- focused on No. 2
- if we have a solution to No. 2 that insures a more steady cash flow greather risks may be taken on No. 1

*** To Buy New Land
- have offer to buy neigbouring vineyard
- costs: £60'000
  - mortgage: £800 per month for 20 years
    - total: £192'000
- 10 acres
- would have to be planted for £5'000
- unknows projected yields

*** What To Do With The Shop
- currently free
- 20'000 visitors per year
- 50% of sales

** The Solution: Wine-O-Mat
*** Idea
- have option to pay £10 to enter shop with free tasting of Wine-O-Mat wine

*** What Does It Do?
- finds the right wine for you
- answer a few questions about yourself and the wines you drink
- taste 2 wines and rate them
- get the recommendation for free or for £30 per case

*** Demo

*** What will it get us?
- modest estimates
- say 1/4 visitors will do the test
- 1/4 of them will buy a case
- costs about 12000 bottles of wine
- experience can be provided unrelated to the weather
- costs scale with use
- very measureable
  - see how many people use the service
  - esay to adjust to new specifications/situation

**** avarage year
- costs: £352'000
- revenue: £422'000
- earings: £70'000
- difference: £92'000

**** bad year
- costs: £304'000
- revenue: £186'000
- loss: £113'000
- difference: £56'000

**** perfect year
- costs: £400'000
- revenue: £615'000
- earnings: £215'000

*** What will it cost us?
- about £2'000 to £3'000 if done by an avarage consultancy

*** Extension
- online shop
  - would set them apart

* App
** Goals
- demonstrate idea
- let people play with it

** Design
- fit winemaker
- simple, elegant
- maybe use red as a base color
- comfy

** Process
- answer a few questions about yourself and the wines you drink
- get a wine recommendation

** Questions

* Handout
** How?
- stuff that wouldn't fit in the presentation
- /not a summary/

** What?
- solution to land problem
- possible entensions to App
